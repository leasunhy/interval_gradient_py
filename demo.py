import numpy as np
import scipy
from PIL import Image
from interval_gradient.interval_filtering import interval_filtering

if __name__ == '__main__':
    # img_obj = Image.open('fish.jpg')
    # img = np.array(img_obj)
    img = scipy.misc.imread('fish.bmp')
    sigma = 3
    res = interval_filtering(img, sigma)
