import math
import numpy as np
import scipy.ndimage
import matplotlib.pyplot as plt

from .gaussian_filter import gaussian_filter

def rescale_gradient(I, ss, grad_type, do_normalize, I0):
    fr = math.ceil(3 * ss)
    k = None
    if grad_type == 'gaussian':
        kl = np.arange(-fr + 1, 1)
        kl = np.exp(-0.5 * (kl / ss) ** 2)
        kl = kl / sum(kl)
        k = np.asarray([[0, *-kl, *kl[::-1]]])

        kg = np.zeros((1, len(kl) * 2 - 1))
        for i in range(0, len(kl)):
            kg[i:len(kl)-i] = kg[i:len(kl)-i] + kl[i]
    # elif grad_type == 'box':
    #     frb = math.ceil(2 * ss)
    #     kl = np.ones(1, frb)
    #     k = [0, *-kl, *kl]
    #     k = k / np.sum(np.abs(k)) * 2
    # elif grad_type == 'dog':
    #     x = np.arange(-fr, fr)
    #     k = np.exp(-0.5 * (x / ss) ** 2)
    #     k = x * k / (ss ** 2)
    #     k = k / np.sum(np.abs(k)) * 2
    #     k[1:fr+1] = k[0:fr]
    #     k[0] = 0

    #     kg = np.zeros((1, len(k) - 2))
    #     for i in range(0, math.floor(len(kg) / 2) + 1):
    #         kg[i:len(kg)-i] = kg[i:len(kg)-i] - k[i + 1]
    # elif 'rtv':
    #     r_gx, wx = rescale_gradient_rtv(I, ss, do_normalize)
    #     return (r_gx, wx)
    else:
        print('unknown interval gradient type, using gaussian.')
        kl = np.arange(-fr + 1, 0)
        kl = np.exp(-0.5 * (kl / ss) ** 2)
        kl = kl / sum(kl)
        k = np.asarray([[0, *-kl, *kl[::-1]]])

    ky = gaussian_filter((2 * fr + 1, 1), ss)
    gx = scipy.ndimage.correlate(I, np.asarray([[0, -1, 1]])[..., np.newaxis], mode='nearest')
    px = scipy.ndimage.correlate(I, k[..., np.newaxis], mode='nearest')

    nch = I.shape[2]

    midx = np.argmax(np.abs(gx), axis=2)

    hh, ww, _ = gx.shape
    # pidx = np.arange(0, hh * ww).reshape((hh, ww))
    # midx = midx * hh * ww + pidx

    # mean_px = np.take(px, midx)  # TODO: confirm we don't need transpose here
    # mean_gx = np.take(gx, midx)

    # TODO: performance
    mean_px = np.zeros((hh, ww))
    mean_gx = np.zeros((hh, ww))
    for i in range(hh):
        for j in range(ww):
            mean_px[i, j] = px[i, j, midx[i, j]]
            mean_gx[i, j] = gx[i, j, midx[i, j]]
    
    err = 1e-4

    abs_gx = np.abs(mean_gx) + err
    abs_px = np.abs(mean_px) + err

    w = abs_px / abs_gx
    w = np.minimum(w, 1)

    pidx = mean_gx * mean_px <= 0
    r_gx = gx * w[..., np.newaxis]
    r_gx[np.tile(pidx[..., np.newaxis], (1, 1, nch))] = 0

    if do_normalize:
        gx0 = scipy.mdimage.correlate(I0, (0, -1, 1), mode='nearest')
        p = gaussian1d(np.take(gx, midx), ss)
        q = gaussian1d(np.take(gx0, midx), ss)

        a = (p * q + err) / (p * p + err)
        a = np.minimum(a, 1)
        a = gaussian1d(a, ss)
        r_gx = r_gx * a

    wx = np.tile(w[..., np.newaxis], (1, 1, nch))

    return (r_gx, wx)