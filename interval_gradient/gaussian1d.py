import math
import numpy as np
import scipy.ndimage

def gaussian1d(I, ss):
    fr = math.ceil(3 * ss)
    gfilter = gaussian_filter((1, 2 * fr + 1), ss)
    return scipy.ndimage.correlate(I, gfilter, mode='reflect')  // or 'mirror'?
