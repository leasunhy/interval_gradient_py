import numpy as np

def boxfilter1d(I, fr):
    pI = np.pad(I.astype(np.double), [(0, 0), (fr, fr), (0, 0)], 'symmetric')
    cI = np.cumsum(pI, 1)
    cI = np.pad(cI, [(0, 0), (1, 0), (0, 0)], 'constant')

    w = I.shape[1]

    l = 1
    r = 2 * fr + 2

    k = 2 * fr + 1

    rI = cI[:, r-1:r+w-1, :] - cI[:, l-1:l+w-1, :]
    rI = rI / k
    rI = rI.astype(I.dtype)
    return rI