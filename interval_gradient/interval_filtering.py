import math
import numpy as np
import matplotlib.pyplot as plt

from .rescale_gradient import rescale_gradient
from .guided_reconstruct_1d import guided_reconstruct_1d

def interval_filtering(img, ss):
    img = img.astype(np.float) / 255.
    I = img

    sigma_s = ss
    epsi = 0.03 ** 2
    grad_type = 'gaussian'
    Nd = 3
    N = 8

    S = I
    wx_prev = np.zeros(S.shape).astype(np.float)
    wy_prev = np.zeros(S.shape).astype(np.float)
    wy_prev = np.transpose(wy_prev, [1, 0, 2])

    total_iter = 0

    for ii in range(0, N):
        gx, wx = rescale_gradient(S, sigma_s, grad_type, False, I)
        gy, wy = rescale_gradient(np.transpose(S, [1, 0, 2]), sigma_s, grad_type, False, np.transpose(I, [1, 0, 2]))
        ngx = gx
        ngy = gy

        dwx = (wx - wx_prev) ** 2
        dwy = (wy - wy_prev) ** 2
        mean_dwx = np.mean(dwx)
        mean_dwy = np.mean(dwy)

        if min(mean_dwx, mean_dwy) <= 0.002:
            break

        S = I
        for i in range(0, Nd):
            ss_i = sigma_s * 3 * math.sqrt(3) * 2 ** (Nd - i - 1) / math.sqrt(4 ** Nd - 1)
            ss_i = round(ss_i)

            if ss_i == 0:
                break
            
            S, _, _, _ = guided_reconstruct_1d(S, ngx, wx, ss_i, epsi)
            S = np.transpose(S, [1, 0, 2])
            S = np.maximum(0, np.minimum(S, 1))

            S, _, _, _ = guided_reconstruct_1d(S, ngy, wy, ss_i, epsi)
            S = np.transpose(S, [1, 0, 2])
            S = np.maximum(0, np.minimum(S, 1))

        wx_prev = wx
        wy_prev = wy

        # plt.figure(101)
        # plt.imshow(S)
        # plt.show()

        total_iter = total_iter + 1

    # plt.figure(101)
    # plt.imshow(S)
    # plt.show()
    print('total_iter = ', total_iter)

    return S

