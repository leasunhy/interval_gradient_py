import numpy as np
import matplotlib.pyplot as plt

from .boxfilter1d import boxfilter1d

def guided_reconstruct_1d(g, gx, wx, sigma_s, epsi):
    ss = sigma_s

    # reconstruct p from gx
    p = g.copy()
    cgx = np.cumsum(gx, 1)
    p[:, 1:, :] = p[:, 0, :][:, np.newaxis, :] + cgx[:, :-1, :]

    q = g

    # 1d guided filter
    mean_w = wx ** 2.
    mean_p = boxfilter1d(p, ss)
    mean_pq = boxfilter1d(p * q, ss)
    mean_q = boxfilter1d(q, ss)
    cov_pq = np.maximum(0, mean_pq - mean_p * mean_q)
    mean_p2 = boxfilter1d(p * p, ss)
    var_p = np.maximum(0, mean_p2 - mean_p ** 2)

    epsi = epsi * mean_w

    a = cov_pq / (var_p + epsi)
    max_a = np.minimum(1, np.amax(a, axis=2))
    max_a = np.tile(max_a[..., np.newaxis], (1, 1, g.shape[2]))
    a = np.maximum(max_a, a)

    b = mean_q - a * mean_p

    mean_a = boxfilter1d(a, ss)
    mean_b = boxfilter1d(b, ss)

    r = mean_a * p + mean_b

    return (r, a, b, p)
